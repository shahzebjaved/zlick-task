import TransactionService from './src/services/TransactionService'
import LoggerService from './src/services/LoggerService'

let doProcess = async () => {
	let results = await TransactionService.process()
	LoggerService.logInformation(results)
}

doProcess()


