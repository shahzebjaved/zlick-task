import { Worker } from 'worker_threads'
import LoggerService from '../services/LoggerService'

class WorkerPool<ScriptReturnType> {

	private workerCount        : number
	private workerScript       : string
	private scriptRunCount     : number
	private workerList         : Worker[] = []
	private workerReturnValues : ScriptReturnType[] = [] 
	private workerReturnErrors : any[] = []
	private promiseResolve     : (type: ScriptReturnType[]) => void
	private promiseReject      : (type: ScriptReturnType[]) => void

	constructor(workerCount: number, workerScript: string, scriptRunCount: number){
		if(scriptRunCount < workerCount) throw Error("Run Counts needs to be more than Worker Count")
		if(workerScript.length == 0)     throw Error("Worker Script need to be not empty string")

		this.workerCount    = workerCount
		this.workerScript   = workerScript
		this.scriptRunCount = scriptRunCount
	}

	public doWork() {
		return new Promise<ScriptReturnType[]>((resolve, reject) => this.initializeWorkers(resolve, reject))
	}

	private initializeWorkers(resolve, reject){
		this.promiseResolve = resolve
		this.promiseReject  = reject
		
		for (let workerId = 0; workerId < this.workerCount; workerId++) {
			let worker = new Worker(this.workerScript)
			worker.on('message', message => this.workerCallback(worker, workerId, message))
			worker.on('error', error => this.workerCallback(worker, workerId, error))
			this.workerList.push(worker)
		}
	}

	private workerCallback(worker: Worker, workerId: number, message: any){
		LoggerService.logInformation(`Worker id: ${workerId}, runCount: ${this.scriptRunCount}`)
		this.decrementScriptRunCount()

		if(this.scriptRunCount < 0) {
			this.workerList.map(worker => worker.terminate())
			if(this.workerReturnErrors.length != 0) this.promiseReject(this.workerReturnErrors.concat(this.workerReturnValues))
			this.promiseResolve(this.workerReturnValues)
			return this.workerReturnValues
		}

		message.error ? this.workerReturnErrors.push(message) : this.workerReturnValues.push(message)

		this.initWorker(worker, workerId)
	}

	private initWorker(worker, workerId){
		worker.removeAllListeners('message')
		worker.removeAllListeners('error')
		worker.on('message', (message) => this.workerCallback(worker, workerId, message))
		worker.on('error', (error) => this.workerCallback(worker, workerId, error))
		worker.postMessage(this.workerScript)
	}

	private decrementScriptRunCount(){
		this.scriptRunCount--
	}
}

export default WorkerPool

