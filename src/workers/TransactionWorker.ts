import FetchTransactionService from '../services/FetchTransactionService'
import { parentPort } from 'worker_threads'
import { AxiosResponse, AxiosError } from 'axios'

class TransactionWorker {

	public static async doWork() {
		let transaction;
		try{
			transaction = await FetchTransactionService.fetchTransaction()
			parentPort.postMessage(transaction)

		} catch (error) {
			let workerError = {error: true, details: (error as AxiosError).code} 
			console.log("error from worker:", workerError)
			parentPort.postMessage(workerError)
		}

		return transaction
	}
}

export default TransactionWorker

parentPort.on('message', () => TransactionWorker.doWork())
TransactionWorker.doWork()
