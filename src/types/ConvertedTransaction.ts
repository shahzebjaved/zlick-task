import Currency from './Currency'

type ConvertedTransaction = {
	createdAt: string,
	currency: Currency,
	convertedAmount: number,
	checksum: string
}

export default ConvertedTransaction