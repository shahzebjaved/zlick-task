import Currency from './Currency'

type Transaction = {
	createdAt: string,
	currency: Currency,
	amount: number,
	exchangeUrl: string,
	checksum: string
}

export default Transaction