import Currency from './Currency'
import ExchangeRate from './ExchangeRate'

type ExchangeRateResults = {
	rates: ExchangeRate,
	base: Currency,
	date: string
}

export default ExchangeRateResults