type PostTransactionReponse = {
	"success": boolean,
 	"passed": number,
	"failed": number
}

export default PostTransactionReponse