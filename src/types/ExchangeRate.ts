import Currency from './Currency'

export type ExchangeRate = {
	Currency : number
}

export default ExchangeRate