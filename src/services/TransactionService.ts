import Transaction from '../types/Transaction'
import Currency, { CurrencyList } from '../types/Currency'
import ConvertedTransaction from '../types/ConvertedTransaction'
import TransactionConverterService from './TransactionConverterService'
import PostTransactionResponse from '../types/PostTransactionResponse'
import FetchTransactionService from './FetchTransactionService'
import LoggerService from './LoggerService'
import Axios from 'axios'

class TransactionService {

	public static readonly PROCESS_TRANSACTIONS_API : string = "https://7np770qqk5.execute-api.eu-west-1.amazonaws.com/prod/process-transactions"
	
	public static async process(transactionCount: number = 100, baseCurrency: Currency = "EUR") {
		let transactions : Transaction[] = await FetchTransactionService.fetchTransactions(transactionCount)
		if(transactions.length == 0) return transactions

		let convertedTransactions : ConvertedTransaction[] = await TransactionConverterService.convertTransactions(transactions, baseCurrency)
		if(convertedTransactions.length == 0) return convertedTransactions

		let postTransactionResult : PostTransactionResponse = await TransactionService.postConvertedTransactions(convertedTransactions)
		LoggerService.logInformation(convertedTransactions)

		return postTransactionResult
	}

	private static async postConvertedTransactions(convertedTransactions: ConvertedTransaction[]) {
		let requestBody = { transactions: convertedTransactions }
		let response    = await Axios.post(TransactionService.PROCESS_TRANSACTIONS_API, requestBody)
		let result      = response.data

		return result
	}

	public static buildTransaction(attrs: any) : Transaction {
		if(attrs == null) throw new Error("Invalid Transaction Attributes.")

		let transaction : Transaction =  {
			createdAt   : attrs.createdAt,
			currency    : attrs.currency,
			amount      : attrs.amount,
			exchangeUrl : attrs.exchangeUrl,
			checksum    : attrs.checksum
		}

		if(!CurrencyList.includes(transaction.currency)) throw new Error(`Invalid Currency ${transaction.currency}, not included in list ${CurrencyList}`)
		if(transaction.createdAt == null || transaction.currency == null || transaction.amount == null || transaction.exchangeUrl == null || transaction.checksum == null) throw new Error(`Invalid Transaction Data`)

		return transaction	
	} 
}

export default TransactionService