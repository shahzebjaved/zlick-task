import TransactionService from './TransactionService'
import Transaction from '../types/Transaction'
import WorkerPool from '../workers/WorkerPool'
import LoggerService from './LoggerService'
import Axios from 'axios'

class FetchTransactionService {

	public static WORKER_COUNT : number = 8

	public static readonly GET_TRANSACTIONS_API : string = "https://7np770qqk5.execute-api.eu-west-1.amazonaws.com/prod/get-transaction"
	public static readonly WORKER_SCRIPT        : string = "./build/src/workers/TransactionWorker.js"

	public static async fetchTransaction(): Promise<Transaction> {
		let response    = await Axios.get(FetchTransactionService.GET_TRANSACTIONS_API)
		let transaction = TransactionService.buildTransaction(response.data)
	
		return transaction
	} 

	public static async fetchTransactions(transactionCount: number): Promise<Transaction[]> {
		let threadCount  = FetchTransactionService.WORKER_COUNT
		let threadScript = FetchTransactionService.WORKER_SCRIPT
		let workerPool   = new WorkerPool<Transaction>(threadCount, threadScript, transactionCount)
		let transactions;

		try { transactions = await workerPool.doWork() } 
		catch (errors) { transactions = [] }
  
		return transactions
	} 

	public static async fetchTransactionsSync(transactionCount: number): Promise<Transaction[]> {
		let transactions = []
		for(let currentCount = 0; currentCount < transactionCount; currentCount++){
			let transaction = await FetchTransactionService.fetchTransaction()
			transactions.push(transaction)
			LoggerService.logInformation(`Current Run Count: ${currentCount + 1}`)
		} 
		
		return transactions
	} 

}

export default FetchTransactionService

