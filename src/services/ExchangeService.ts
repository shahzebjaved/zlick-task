import Axios from 'axios'
import Currency from '../types/Currency'
import ExchangeRateResults from '../types/ExchangeRateResults'

class ExchangeService {

	public static readonly EXCHANGE_API_BASE_URL : string = "https://api.exchangeratesapi.io"
	private static instance : ExchangeService 

	private baseCurrency        : Currency
	private exchangeRatesToBase : ExchangeRateResults

	public static async getService(baseCurrency: Currency): Promise<ExchangeService> {
		if (ExchangeService.instance == null) ExchangeService.instance = new ExchangeService()	
		if (ExchangeService.instance.getBaseCurrency() != baseCurrency) await ExchangeService.instance.setBaseCurrency(baseCurrency)

		return ExchangeService.instance
	}

	public getExchageRates(){
		return this.exchangeRatesToBase
	}

	public async setBaseCurrency(baseCurrency: Currency) {
		this.baseCurrency        = baseCurrency
		this.exchangeRatesToBase = await this.fetchExchangeRates()
	}

	public getBaseCurrency(): Currency {
		return this.baseCurrency
	}

	public getExchangedCurrency(currentCurrency: Currency, amount: number): number {
		let currentCurrencyToBaseRate = this.exchangeRatesToBase[currentCurrency]
		let convertedAmount           = amount / currentCurrencyToBaseRate

		return parseFloat(convertedAmount.toFixed(4))
	}

	private async fetchExchangeRates(): Promise<ExchangeRateResults> {
		let ratesResult = await Axios.get(this.fetchExchangeRatesUrl())
		if(ratesResult.status != 200) return null

		return ratesResult.data.rates
	}

	private fetchExchangeRatesUrl(): string {
		return `${ExchangeService.EXCHANGE_API_BASE_URL}/${this.fetchExchangeDate()}?base=${this.baseCurrency}`
	}

	private fetchExchangeDate(): string {
		let date = new Date()
		return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
	}
}

export default ExchangeService

