import Transaction from '../types/Transaction'
import ConvertedTransaction from '../types/ConvertedTransaction'
import Currency from '../types/Currency'
import ExchangeService from './ExchangeService'
import LoggerService from './LoggerService'

class TransactionConverterService {

	public static async convertTransactions(transactions: Transaction[], baseCurrency: Currency): Promise<ConvertedTransaction[]> {
		let exchangeService : ExchangeService = await ExchangeService.getService(baseCurrency)
		if(exchangeService.getExchageRates() == null) return []

		let convertedTransactions : ConvertedTransaction[] = transactions.map((transaction : Transaction) => TransactionConverterService.convertTransaction(transaction, exchangeService))
		
		return convertedTransactions
	}

	private static convertTransaction(transaction: Transaction, exchangeService: ExchangeService): ConvertedTransaction {
		let convertedAmount      = exchangeService.getExchangedCurrency(transaction.currency, transaction.amount)
		let convertedTransaction = TransactionConverterService.buildConvertedTransaction(transaction, convertedAmount)
		
		return convertedTransaction
	}

	private static buildConvertedTransaction(transaction: Transaction, convertedAmount: number): ConvertedTransaction {
		return { 
			createdAt: transaction.createdAt, 
			currency: transaction.currency, 
			convertedAmount: convertedAmount, 
			checksum: transaction.checksum
		}
	}
}

export default TransactionConverterService