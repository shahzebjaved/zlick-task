type Callback = (message: any) => void

let parentEvents : Map<string, Callback[]> = new Map<string, Callback[]>()
let workerEvents : Map<string, Callback[]> = new Map<string, Callback[]>()
let transaction = { 
	createdAt   : "2019-02-27T19:34:41.695Z", 
	currency    : "THB",
	amount      : 847, 
	exchangeUrl : "https://api.exchangeratesapi.io/Y-M-D?base=EUR",
	checksum    : "5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"
}
let workerError = { error: true, details: "AddsNotFound"}
let messageCounter = 0

export class Worker {

	private active: boolean

	constructor(){
		this.active = true
	}

	on(message: any, callback: Callback){
		let newCallback = () => {
			if(!this.active) return
			this.active = false
			messageCounter > 5 ? callback(workerError) : callback(transaction)
		}
		let callbacks = parentEvents.get(message)
		if(callbacks == null) callbacks = []
		callbacks.push(newCallback)
		parentEvents.set(message, callbacks)
	}

	removeAllListeners(message: string){
		parentEvents.delete(message)
	}

	postMessage(message: any){
		this.active = true
		let messageCallbacks = workerEvents.get('message')
		if(messageCallbacks == null) return 
		messageCallbacks.forEach(callback => callback(message))
	}

	terminate(){
		workerEvents = null
		this.active = false
	}
}

export class parentPort {

	static on(message: string, callback) {
		let callbacks = workerEvents.get(message)
		if(callbacks == null) callbacks = []
		callbacks.push(callback)
		workerEvents.set(message, callbacks)
	}

	static postMessage(message: any){
		let parentCallback = parentEvents.get('message')
		if(parentCallback == null) return

		parentCallback.forEach(callback => callback(message))
	}
}

setInterval(() => {
	messageCounter++
	parentPort.postMessage('')
}, 10)

