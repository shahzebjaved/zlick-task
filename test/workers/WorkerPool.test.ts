import axios from 'axios'
import WorkerPool from '../../src/workers/WorkerPool'
import Transaction from '../../src/types/Transaction'
import FetchTransactionService from '../../src/services/FetchTransactionService'
jest.mock('worker_threads')
jest.mock('axios')
jest.mock('../../src/services/FetchTransactionService', () => ({ fetchTransaction: jest.fn() }))

describe("WorkerPool", () => {

	test("doWork", async () => {
		let workerPool : any = new WorkerPool<string>(4, '../../build/src/workers/TransactionWorker', 10)
		let transactions: any[];

		try{ transactions = await workerPool.doWork() }
		catch (errors){ transactions = errors }

		expect(workerPool.workerList.length).toEqual(4)
		expect(transactions.length).toEqual(10)
		
		let withoutError = transactions.filter(transaction => !transaction.error)
		
		expect(withoutError.length).toEqual(8)
		expect(withoutError[0].checksum).toEqual('5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04')	
	})
})
