import axios from 'axios'
import TransactionWorker from '../../src/workers/TransactionWorker'
import FetchTransactionService from '../../src/services/FetchTransactionService'
import Transaction from '../../src/types/Transaction'
jest.mock('axios')
jest.mock('worker_threads')
jest.mock('../../src/services/FetchTransactionService', () => ({ fetchTransaction: jest.fn() }))

describe("TransactionWorker", () => {
	let transaction : Transaction = { "createdAt":"2019-02-27T19:34:41.695Z", "currency":"THB","amount":847,"exchangeUrl":"https://api.exchangeratesapi.io/Y-M-D?base=EUR","checksum":"5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"};
	(FetchTransactionService.fetchTransaction as jest.Mock).mockImplementation(() => Promise.resolve(transaction))
		
	test("doWork", async () => {
		let returnVal = await TransactionWorker.doWork();

		expect(returnVal).toEqual(transaction)
		expect((FetchTransactionService.fetchTransaction as jest.Mock).mock.calls.length).toBe(2)
	})

	// test("handles errors in workers", () => {
	// 	(FetchTransactionService.fetchTransaction as jest.Mock).mockImplementation(() => )
	// })
})