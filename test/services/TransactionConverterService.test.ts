import TransactionConverterService from '../../src/services/TransactionConverterService'
import TransactionService from '../../src/services/TransactionService'
import axios from 'axios'
import ExchangeService from '../../src/services/ExchangeService'
jest.mock('../../src/services/ExchangeService', () => ({ getExchageRates: jest.fn(), getService: jest.fn() }))
jest.mock('axios')
jest.mock('worker_threads')

describe("TransactionConverterService", () => {

	beforeEach(() => {
	  jest.resetAllMocks();
	  jest.resetAllMocks();
	});

	let transactions = [ { createdAt: '2019-02-27T19:34:41.695Z',
        currency: 'INR',
        amount: 847,
        exchangeUrl: 'https://api.exchangeratesapi.io/Y-M-D?base=EUR',
        checksum:
         '5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04' },
      { createdAt: '2019-02-27T19:34:41.695Z',
        currency: 'INR',
        amount: 123,
        exchangeUrl: 'https://api.exchangeratesapi.io/Y-M-D?base=EUR',
        checksum:
         '5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04' },
      { createdAt: '2019-02-27T19:34:41.695Z',
        currency: 'INR',
        amount: 234,
        exchangeUrl: 'https://api.exchangeratesapi.io/Y-M-D?base=EUR',
        checksum:
         '5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04' },
    ].map(transaction => TransactionService.buildTransaction(transaction));

    test("convertTransactions", async () => {

    	let mockGetService = jest.spyOn(ExchangeService as any, 'getService');
		mockGetService.mockImplementationOnce(() => Promise.resolve({ getExchageRates: (s: string) => ({ 'INR': 81.7685}), getExchangedCurrency: () => 1223.3 }));

		let typedTransactions = transactions
		let convertedTransactions = await TransactionConverterService.convertTransactions(transactions, "EUR")

		let convertedAmounts = convertedTransactions.map(convertedTransaction => convertedTransaction.convertedAmount)

		expect(convertedAmounts).toEqual([1223.3,1223.3,1223.3])
	})

	test("check for exception in echange api", async () => {
		let mockGetService = jest.spyOn(ExchangeService as any, 'getService');
		mockGetService.mockImplementationOnce(() => Promise.resolve({ getExchageRates: (s: string) => null }));
		
		let typedTransactions = transactions
		let convertedTransactions = await TransactionConverterService.convertTransactions(transactions, "EUR")

		expect(convertedTransactions.length).toEqual(0)
	})
})