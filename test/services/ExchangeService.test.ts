import ExchangeService from '../../src/services/ExchangeService'
import Currency from '../../src/types/Currency'
import axios from 'axios'
jest.mock('axios')

describe('ExchangeService.getService()', () => {

	beforeEach(() => {
		(axios.get as jest.Mock).mockReset()
	}) 

	test("getExchangedCurrency", async () => {
		let apiRates ={"rates":{"INR":81.7685},"base":"EUR","date":"2020-05-07"};
		(axios.get as jest.Mock).mockImplementationOnce(() => Promise.resolve({ status: 200, data: apiRates}))
		let exchangeService = await ExchangeService.getService("EUR")
		let exchangedAmount = parseFloat((200 / apiRates['rates']['INR']).toFixed(4)) 

		expect(exchangeService.getExchangedCurrency("INR", 200)).toEqual(exchangedAmount)
	})

	test("fetchExchangeRates", async () => {
		let apiRates ={"rates":{"INR":81.7685},"base":"EUR","date":"2020-05-07"};
		(axios.get as jest.Mock).mockImplementationOnce(() => Promise.resolve({ status: 200, data: apiRates}))
		let exchangeService : any = await ExchangeService.getService("EUR")
		expect(JSON.stringify(exchangeService.exchangeRatesToBase)).toBe(JSON.stringify({ "INR":81.7685}))

		apiRates ={"rates":{"INR":1821.7685},"base":"USD","date":"2020-05-07"};
		(axios.get as jest.Mock).mockImplementationOnce(() => Promise.resolve({ status: 200, data: apiRates}))
		await exchangeService.setBaseCurrency("USD")
		expect(JSON.stringify(exchangeService.exchangeRatesToBase)).toBe(JSON.stringify({"INR":1821.7685}))
	})

	test("fetchExchangeDate", async () => {
		let apiRates ={"rates":{"INR":81.7685},"base":"EUR","date":"2020-05-07"};
		(axios.get as jest.Mock).mockImplementationOnce(() => Promise.resolve({ status: 200, data: apiRates}))
		let exchangeService : any = await ExchangeService.getService("EUR")
		let date = new Date()
		let dateString = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`

		expect(exchangeService.fetchExchangeDate()).toEqual(dateString)
	})

	test("return empty rates if api return error", async () => {
		(axios.get as jest.Mock).mockImplementationOnce(() => Promise.resolve({ status: 400, data: null }))
		let exchangeService : any = await ExchangeService.getService("EUR")

		await exchangeService.setBaseCurrency('EUR')

		expect(exchangeService.exchangeRatesToBase).toBe(null)

	})
})

