import axios from 'axios'
import TransactionService from '../../src/services/TransactionService'
import FetchTransactionService from '../../src/services/FetchTransactionService'
import TransactionConverterService from '../../src/services/TransactionConverterService'
jest.mock('axios')
jest.mock('../../src/services/FetchTransactionService', () => ({ fetchTransactions: jest.fn() }));
jest.mock('../../src/services/TransactionConverterService', () => ({ convertTransactions: jest.fn() }));

describe("TransactionService", () => {

	(axios.post as jest.Mock).mockImplementation(() => Promise.resolve({ status: 200, data: { success: false, passed: 0, failed: 100 }}))

	test("process", async () => {
		let postConvertedTransactionsSpy = jest.spyOn(TransactionService as any, "postConvertedTransactions")
		let fetchTransactionsSpy = jest.spyOn(FetchTransactionService as any, 'fetchTransactions')
		fetchTransactionsSpy.mockImplementationOnce(() => Promise.resolve([{"createdAt":"2019-02-27T19:34:41.695Z","currency":"THB","amount":847,"exchangeUrl":"https://api.exchangeratesapi.io/Y-M-D?base=EUR","checksum":"5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"}]))
		let convertTransactionsSpy = jest.spyOn(TransactionConverterService as any, 'convertTransactions')
		convertTransactionsSpy.mockImplementationOnce(() => Promise.resolve([{"createdAt":"2019-02-27T19:34:41.695Z","currency":"THB","amount":847,"exchangeUrl":"https://api.exchangeratesapi.io/Y-M-D?base=EUR","checksum":"5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"}]))

		let postResult = await TransactionService.process();
		
		expect((FetchTransactionService.fetchTransactions as jest.Mock).mock.calls.length).toBe(1)
		expect((TransactionConverterService.convertTransactions as jest.Mock).mock.calls.length).toBe(1)
		expect(postConvertedTransactionsSpy.mock.calls.length).toBe(1)
	})

	test("buildTransaction", () => {
		let transactionAttrs1 = {"createdat":"2019-02-27T19:34:41.695Z","currency":"THB","amount":847,"exchangeUrl":"https://api.exchangeratesapi.io/Y-M-D?base=EUR","checksum":"5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"}
		let transactionAttrs2 = {"createdAt":null,"currency":"THB","amount":847,"exchangeUrl":"https://api.exchangeratesapi.io/Y-M-D?base=EUR","checksum":"5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"}
		let transactionAttrs3 = {"createdAt":"2019-02-27T19:34:41.695Z","currency":"ABC","amount":847,"exchangeUrl":"https://api.exchangeratesapi.io/Y-M-D?base=EUR","checksum":"5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"}
		let transactionAttrs4 = {"createdAt":"2019-02-27T19:34:41.695Z","currency":"THB","amount":847,"exchangeUrl":"https://api.exchangeratesapi.io/Y-M-D?base=EUR","checksum":"5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"}

		expect(() => TransactionService.buildTransaction(transactionAttrs1)).toThrow(Error)
		expect(() => TransactionService.buildTransaction(transactionAttrs2)).toThrow(Error)
		expect(() => TransactionService.buildTransaction(transactionAttrs3)).toThrow(Error)
		expect(() => TransactionService.buildTransaction(transactionAttrs4)).not.toThrow(Error)
	})
}) 