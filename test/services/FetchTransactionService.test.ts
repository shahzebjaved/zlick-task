import axios from 'axios'
import Transaction from '../../src/types/Transaction'

import FetchTransactionService from '../../src/services/FetchTransactionService'
jest.mock('axios')
jest.mock('worker_threads')

describe("FetchTransactionService", () => {
	let transactionResponse = {"createdAt":"2019-02-27T19:34:41.695Z","currency":"THB","amount":847,"exchangeUrl":"https://api.exchangeratesapi.io/Y-M-D?base=EUR","checksum":"5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04"};
	(axios.get as jest.Mock).mockImplementation(() => Promise.resolve({ status: 200, data: transactionResponse}))

	test("fetchTransaction", async () => {		
		let transaction = await FetchTransactionService.fetchTransaction()
		expect(transaction.checksum).toBe("5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04")
	})

	test("fetchTransactions", async () => {
		let transactions = await FetchTransactionService.fetchTransactions(10)

		expect(transactions.length).toEqual(10)
		expect(transactions[0].checksum).toEqual('5d16c50c677c7318c0572ffb0fa20794dbe94266d3523ac7899e6efaba2e6c04')
	})
})